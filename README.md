# Blender Copy UE4 Transforms

A simple Blender addon for copying object transform inside blender to UE4 clipboard syntax. This addon aims to ease scene tranform from Blender worldspace to UE4 worldspace.

## Features

* Options to copy either object's translation, rotation or scale
* Optional translation/rotational offset
* Scale override

## Installation

* Download this repository as `.zip` file
* In Blender, click on `Edit` > `Preferences`, under `Add-ons` tab, click install and point it to the downloaded file

## Usage

* A new `Copy` tab will appear in the right side of the 3D viewport
* Select any object in the scene
* Click `Copy Object(s) Transform`
* Paste into UE4 viewport
* Replace the default cube static mesh with your asset
