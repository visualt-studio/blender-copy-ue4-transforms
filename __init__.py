# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Copy Transform to UE4",
    "author" : "drpsyko101@gmail.com",
    "description" : "Copy object transform from blender to UE4 format.",
    "blender" : (2, 80, 0),
    "version" : (0, 1, 13),
    "location" : "",
    "warning" : "",
    "category" : "UE4"
}

import bpy
import math

class copy_property(bpy.types.PropertyGroup):
    translation : bpy.props.BoolProperty(name= "Translation", default= True)
    translation_offset : bpy.props.FloatVectorProperty(
        name= "Translation Offset",
        description= "Offset rotation on top of existing object rotation.",
        subtype= "TRANSLATION",
        default= (0,0,0)
    )
    rotation : bpy.props.BoolProperty(name= "Rotation", default= True)
    rotation_offset : bpy.props.FloatVectorProperty(
        name= "Rotation Offset",
        description= "Offset rotation on top of existing object rotation.",
        subtype= "EULER",
        default= (0,0,0)
    )
    scale : bpy.props.BoolProperty(name= "Scale", default= True)
    scale_override : bpy.props.FloatProperty(
        name= "Scale",
        description= "Override scale factor.",
        default= 1
    )

class OBJECT_PT_CopyTool(bpy.types.Panel):
    bl_idname = "view3d.copy_PT_panel"
    bl_label = "Copy Transform"
    bl_category = "Copy"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        layout = self.layout
        
        t = bpy.context.scene.copy_property
        
        translation_column = layout.column()
        translation_column.prop(t, "translation", text = "Copy Translation")

        if t.translation == True: # if true, show translation offset slider
            translation_column.prop(t, "translation_offset")

        rotation_column = layout.column()
        rotation_column.prop(t, "rotation", text = "Copy Rotation")

        if t.rotation == True: # if true, show rotation offset slider
            rotation_column.prop(t, "rotation_offset")
            
        scale_column = layout.column()
        scale_column.prop(t, "scale", text = "Copy Scale")
        
        if t.scale == True: # if true, show scale override slider
            scale_column.prop(t, "scale_override", text = "Scale Override")
        
        row = layout.row()
        row.operator("wm.copytool", text="Copy Object(s) Transform", icon='DUPLICATE')

class WM_OT_CopyTool(bpy.types.Operator):
    bl_idname = "wm.copytool"
    bl_label = "Copy Transform to UE4"
    bl_description = "Copy object transform to UE4 scene format"

    def execute(self, context):
        unit = bpy.data.scenes["Scene"].unit_settings.scale_length # get scene unit value

        t = bpy.context.scene.copy_property


        begin_map_template = "Begin Map\n\tBegin Level\n"
        begin_actor_template = "\t\tBegin Actor Class=/Script/Engine.StaticMeshActor Name=TestName Archetype=/Script/Engine.StaticMeshActor\'/Script/Engine.Default__StaticMeshActor\'\n\t\t\tBegin Object Class=/Script/Engine.StaticMeshComponent Name=\"StaticMeshComponent0\" Archetype=StaticMeshComponent\'/Script/Engine.Default__StaticMeshActor:StaticMeshComponent0\'\n\t\t\tEnd Object\n\t\t\tBegin Object Name=\"StaticMeshComponent0\"\n\t\t\t\tStaticMesh=StaticMesh\'\"/Engine/BasicShapes/Cube.Cube\"\'\n\t\t\t\tStaticMeshDerivedDataKey=\"STATICMESH_A9B55814188440DA96CF4A9D318BC1C5_228332BAE0224DD294E232B87D83948FQuadricMeshReduction_V1$2e0_26D666F86459324BE77D4F6A1939C6698000000000100000001000000000000000100000001000000010000000000000000000000000000004000000000000000020000000000803F0000803F0000803F0000004000000000050000004E6F6E650030000000803F0000803F000000000000004100000000000034420303030000000000000000LS0MNSzzzzzzzz0\"\n\t\t\t\tOverrideMaterials(0)=Material\'\"/Engine/BasicShapes/BasicShapeMaterial.BasicShapeMaterial\"\'\n"
        end_actor_template = "\t\t\tEnd Object\n\t\t\tStaticMeshComponent=\"StaticMeshComponent0\"\n\t\t\tRootComponent=\"StaticMeshComponent0\"\n\t\t\tActorLabel="
        end_map_template = "\tEnd Level\nBegin Surface\nEnd Surface\nEnd Map"

        objects_name = []
        clipboard = []

        for obj in bpy.context.selected_objects:
            objects_name.append(obj.name) # make obj.name print array

            loc = [0.0,0.0,0.0]
            rot = [0.0,0.0,0.0]
            scl = [1.0,1.0,1.0]

            if t.translation == True:
                loc = obj.location * 100 * unit + t.translation_offset # blender default unit is in meters

            if t.rotation == True:
                rot = [math.degrees(obj.rotation_euler[1] + t.rotation_offset[1]) * -1, math.degrees(obj.rotation_euler[2] + t.rotation_offset[2]) * -1, math.degrees(obj.rotation_euler[0] + t.rotation_offset[0])]

            if t.scale == True:
                scl = obj.scale * t.scale_override
                
            print(obj.rotation_euler)

            clipboard.append("{}\t\t\t\tRelativeLocation=(X={},Y={},Z={})\n\t\t\t\tRelativeRotation=(Pitch={},Yaw={},Roll={})\n\t\t\t\tRelativeScale3D=(X={},Y={},Z={})\n{}\"{}\"\n\t\tEnd Actor\n".format(begin_actor_template, loc[0] * unit, loc[1] * unit * -1, loc[2]  * unit, rot[0], rot[1], rot[2], scl[0], scl[1], scl[2], end_actor_template, obj.name))

        bpy.context.window_manager.clipboard = begin_map_template + '\n'.join(clipboard) + end_map_template

        print(f'{objects_name}' + "copied to clipboard.")
        if len(objects_name) == 1:
            self.report({'INFO'}, "{} object copied to clipboard.".format(len(objects_name)))
        elif len(objects_name) > 1:
            self.report({'INFO'}, "{} objects copied to clipboard.".format(len(objects_name)))
        else:
            self.report({'WARNING'}, "No object selected!")

        return {'FINISHED'}

#############################[...]#############################


classes = (

)

def register():
    from bpy.utils import register_class
    bpy.utils.register_class(copy_property)
    bpy.utils.register_class(OBJECT_PT_CopyTool)
    bpy.utils.register_class(WM_OT_CopyTool)
    
    bpy.types.Scene.copy_property = bpy.props.PointerProperty(type=copy_property)

def unregister():
    from bpy.utils import unregister_class
    bpy.utils.unregister_class(copy_property)
    bpy.utils.unregister_class(OBJECT_PT_CopyTool)
    bpy.utils.unregister_class(WM_OT_CopyTool)
    
    del bpy.types.Scene.copy_property

#register, unregister = bpy.utils.register_classes_factory(classes)
